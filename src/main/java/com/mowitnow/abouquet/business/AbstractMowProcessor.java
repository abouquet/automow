package com.mowitnow.abouquet.business;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowInstruction;
import com.mowitnow.abouquet.api.MowPosition;
import com.mowitnow.abouquet.api.MowVector;

/**
 * 
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public abstract class AbstractMowProcessor implements MowProcessor {
	
	protected List<MowInstruction> instructions;
	
	protected static final MowPosition DEFAULT_MAX_AREA_COORDINATE = new MowPosition(5, 5);
	protected MowPosition maxAreaCoordinate = DEFAULT_MAX_AREA_COORDINATE;
	
	protected static final MowVector DEFAULT_CURRENT_VECTOR = new MowVector();
	protected MowVector currentMowerVector = DEFAULT_CURRENT_VECTOR;
	
	public Logger LOG;
	
	public AbstractMowProcessor() {
		LOG = LoggerFactory.getLogger(this.getClass());
	}
	
	public List<MowInstruction> getInstructions() {
		if(instructions == null){
			instructions = new ArrayList<>();
		}
		return instructions;
	}
	
	@Override
	public void addInstruction(MowInstruction instruction){
		if(instructions == null){
			instructions = new ArrayList<>();
		}
		instructions.add(instruction);
	}
	
	protected void checkInstruction() throws MowException {
		if(instructions==null || instructions.isEmpty()){
			String message  = "No instructions given to the processor, abording mowing...";
			LOG.error(message);
			throw new MowException(message);
		}
	}

	@Override
	public void setAreaOfMowing(MowPosition position) {
		maxAreaCoordinate=position;
	}
	
	@Override
	public MowVector getCurrentVector() {
		return currentMowerVector;
	}
	
	@Override
	public void setCurrentVector(MowVector mowVector) {
		currentMowerVector=mowVector;
	}
	
	protected void readVariables() {
		LOG.debug("Current Mow Vector is [{},{},{}]",new Object[]{currentMowerVector.getPosition().getPositionX(),currentMowerVector.getPosition().getPositionY(),currentMowerVector.getOrientation().toString()});
		//LOG.debug("Mower Area of Work is [{},{}]",new Object[]{maxAreaCoordinate.getPositionX(),maxAreaCoordinate.getPositionY()});
	}

}
