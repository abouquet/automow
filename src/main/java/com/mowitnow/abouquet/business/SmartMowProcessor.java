package com.mowitnow.abouquet.business;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowInstruction;
import com.mowitnow.abouquet.api.MowOrientation;
import com.mowitnow.abouquet.api.MowVector;

/**
 * 
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class SmartMowProcessor extends AbstractMowProcessor implements MowProcessor {

	private static final String MESSAGE_OUT_OF_BOUND = "Bounds of area detected, won't go forward...";

	@Override
	public MowVector mow() throws MowException {

		checkInstruction();
		if(LOG.isDebugEnabled()){
			StringBuffer buff = new StringBuffer();
			for(MowInstruction instruction : instructions){
				buff.append(instruction.name());
			}
			LOG.debug("Compiled instructions is {}",buff);
		}
		for(MowInstruction instruction : instructions){
			LOG.info("Executing instruction \"{}\"",instruction.toString());
			readVariables();
			switch (instruction) {
			case A:
				computeForwardInstruction();
				break;
			case D:
				computeRightInstruction();
				break;
			case G:
				computeLeftInstruction();
			default:
				break;
			}
		}
		return currentMowerVector;
	}


	private void computeLeftInstruction() {
		switch(currentMowerVector.getOrientation()){
		case WEST:
			currentMowerVector.setOrientation(MowOrientation.SOUTH);
			break;
		case SOUTH:
			currentMowerVector.setOrientation(MowOrientation.EAST);
			break;
		case EAST:
			currentMowerVector.setOrientation(MowOrientation.NORTH);
			break;
		case NORTH:
			currentMowerVector.setOrientation(MowOrientation.WEST);
			break;
		}
	}


	private void computeRightInstruction() {
		switch(currentMowerVector.getOrientation()){
		case WEST:
			currentMowerVector.setOrientation(MowOrientation.NORTH);
			break;
		case SOUTH:
			currentMowerVector.setOrientation(MowOrientation.WEST);
			break;
		case EAST:
			currentMowerVector.setOrientation(MowOrientation.SOUTH);
			break;
		case NORTH:
			currentMowerVector.setOrientation(MowOrientation.EAST);
			break;
		}
	}


	private void computeForwardInstruction() {
		switch(currentMowerVector.getOrientation()){
		case WEST:
			if(currentMowerVector.getPosition().getPositionX()!=0){
				currentMowerVector.getPosition().setPositionX(currentMowerVector.getPosition().getPositionX()-1);
			}
			else{
				//Do nothing, out of area
				LOG.debug(MESSAGE_OUT_OF_BOUND);
			}
			break;
		case SOUTH:
			if(currentMowerVector.getPosition().getPositionY()!=0){
				currentMowerVector.getPosition().setPositionY(currentMowerVector.getPosition().getPositionY()-1);
			}
			else{
				//Do nothing, out of area
				LOG.debug(MESSAGE_OUT_OF_BOUND);
			}
			break;
		case EAST:
			if(currentMowerVector.getPosition().getPositionX()<maxAreaCoordinate.getPositionX()){
				currentMowerVector.getPosition().setPositionX(currentMowerVector.getPosition().getPositionX()+1);
			}
			else{
				//Do nothing, out of area
				LOG.debug(MESSAGE_OUT_OF_BOUND);
			}
			break;
		case NORTH:
			if(currentMowerVector.getPosition().getPositionY()<maxAreaCoordinate.getPositionY()){
				currentMowerVector.getPosition().setPositionY(currentMowerVector.getPosition().getPositionY()+1);
			}
			else{
				//Do nothing, out of area
				LOG.debug(MESSAGE_OUT_OF_BOUND);
			}
			break;
		}
	}


	@Override
	public String getProcessorName() {
		return "Smart Processor";
	}

}
