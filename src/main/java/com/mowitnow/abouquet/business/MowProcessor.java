package com.mowitnow.abouquet.business;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowInstruction;
import com.mowitnow.abouquet.api.MowPosition;
import com.mowitnow.abouquet.api.MowVector;

/**
 * Interface processor that dictate major mow business rules
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public interface MowProcessor {

	/**
	 * Execute instructions provided to the processor to move the mower
	 * @return Position the final position of the mower after mow has been done
	 * @throws MowException in case of a mow failure
	 */
	public MowVector mow() throws MowException;

	/**
	 * Add an instruction to the processor instruction queue
	 * @param instruction an instruction to send to the processor
	 */
	void addInstruction(MowInstruction instruction);
	
	/**
	 * Retrieve the current vector calculated or set on this processor
	 * @return MowVector the current vector of this processor
	 */
	MowVector getCurrentVector();
	
	/**
	 * Define the current vector of this processor, this should be set once at the beginning of mowing, after that the vector is automatically computed
	 * @param mowVector the current vector to define
	 */
	void setCurrentVector(MowVector mowVector);

	/**
	 * Get a readable name of this processor
	 * @return String the readable name of this processor
	 */
	public String getProcessorName();
	
	/**
	 * Providing coordinates of an area begin to a 0,0 coordinates, the AreaOfMowing provides the right upper point coordinate  defining the opposite point which delimitate a rectangular area of working for the mower
	 * @param position the right upper point delimitating the rectangular area of work of the mower
	 */
	public void setAreaOfMowing(MowPosition position);
}
