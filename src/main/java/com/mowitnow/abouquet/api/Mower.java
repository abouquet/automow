package com.mowitnow.abouquet.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnow.abouquet.business.MowProcessor;

/**
 * Base class of the mower, provide access to a {@link MowProcessor}  interface which dictate mowing rule. <br>
 * Before mowing, the mower should know it {@link MowVector} and it area of processing defined also in the MowProcessor. <br>
 * @author Anthony BOUQUET
 * @since 0.0.1
 * @see MowProcessor
 *
 */
public class Mower   {

	public static final Logger LOG = LoggerFactory.getLogger(Mower.class);
	private static final String DEFAULT_MOWER_NAME = "Rename me !";

	private String mowerName = DEFAULT_MOWER_NAME;
	
	private static final String PROCESSOR_ERROR_MSG = "Mow processor has not been set, you should assign a concrete processor first";

	private MowProcessor processor;

	public void setProcessor(MowProcessor processor) {
		this.processor = processor;
	}

	/**
	 * Make the mower mow, delegate mowing to processor and log the result back here
	 * @return
	 * @throws MowException
	 */
	public MowVector mow() throws MowException{
		if (processor == null ){
			throw new MowException(PROCESSOR_ERROR_MSG);
		}
		if(processor.getCurrentVector() ==null){
			throw new MowException("Mower Vector (location and orientation) have not been set");
		}
		LOG.info("\"{}\" is about to mow...",this.mowerName);
		MowVector mowVector =  processor.mow();
		
		LOG.info("--------------------------------------------------------------");
		LOG.info("Mow Vector of \"{}\" mower after movements is [{},{},{}]",new Object[]{
				getMowerName(),
				mowVector.getPosition().getPositionX(),
				mowVector.getPosition().getPositionY(),
				mowVector.getOrientation().toString()});
		LOG.info("--------------------------------------------------------------");
		return mowVector;
	}

	public String getMowerName() {
		return mowerName;
	}

	public void setMowerName(String mowerName) {
		this.mowerName = mowerName;
	}

	public void setMowVector(MowVector mowVector) {
		processor.setCurrentVector(mowVector);
	}

	public void addInstruction(MowInstruction instruction){
		processor.addInstruction(instruction);
	}
	
	public void setAreaOfMowing(MowPosition maxPosition) throws MowException{
		if(processor!=null){
			processor.setAreaOfMowing(maxPosition);
		}
		else{
			throw new MowException(PROCESSOR_ERROR_MSG);
		}

	}

}
