package com.mowitnow.abouquet.api;

/**
 * Enum used to store orientation types
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public enum MowOrientation {
	NORTH("N"),SOUTH("S"),EAST("E"),WEST("W");
	
	private String orientationCaption;
	
	private MowOrientation(String orientationCaption) {
		this.orientationCaption=orientationCaption;
	}
	
	@Override
	public String toString() {
		return orientationCaption;
	}
}
