package com.mowitnow.abouquet.api;

/**
 * Class used to store a two dimensionnal coordinates
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class MowPosition {
	
	private int positionX;
	private int positionY;
	
	public MowPosition() {
	}
	
	public MowPosition(int positionX,int positionY){
		this.positionX=positionX;
		this.positionY=positionY;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	
}
