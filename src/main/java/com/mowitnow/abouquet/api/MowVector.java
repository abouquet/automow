package com.mowitnow.abouquet.api;

/**
 * Class used to store a position and an orientation which determines a mow vector
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class MowVector {
	
	private MowPosition position = new MowPosition(0, 0);
	private MowOrientation orientation = MowOrientation.NORTH;
	
	public MowPosition getPosition() {
		return position;
	}
	public void setPosition(MowPosition position) {
		this.position = position;
	}
	public MowOrientation getOrientation() {
		return orientation;
	}
	public void setOrientation(MowOrientation orientation) {
		this.orientation = orientation;
	}
}
