package com.mowitnow.abouquet.api;

/**
 * Enum used to store instructions types
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public enum MowInstruction {
	G("To the left"),D("To the right"),A("Move forward");
	
	private String instructionCaption;
	
	private MowInstruction(String instructionCaption) {
		this.instructionCaption=instructionCaption;
	}
	
	@Override
	public String toString() {
		return instructionCaption;
	}
}
