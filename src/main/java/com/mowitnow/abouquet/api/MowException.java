package com.mowitnow.abouquet.api;

/**
 * Base Exception Class for Mower Software
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class MowException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -523059579891555243L;

	public MowException() {
		super();
	}

	public MowException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public MowException(String message, Throwable cause) {
		super(message, cause);
	}

	public MowException(String message) {
		super(message);
	}

	public MowException(Throwable cause) {
		super(cause);
	}
	
	
}
