package com.mowitnow.abouquet.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnow.abouquet.business.MowProcessor;

/**
 * Create multiple mowers at once provided with a MowProcessor implementation. <br>
 * This factory controls the way Mowers are created, it can't create any other mowers if previous swarm mowers hasn't been destroyed
 * 
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class MowSwarmFactory {

	public static final Logger LOG = LoggerFactory.getLogger(MowSwarmFactory.class);

	private static final int DEFAULT_MOWER_NUMBER = 2;
	private static MowSwarmFactory INSTANCE;

	private List<Mower> mowerList;


	public static MowSwarmFactory getInstance(){
		LOG.debug("Retrieving instance of {}",MowSwarmFactory.class.getSimpleName());
		if(INSTANCE==null)
			INSTANCE = new MowSwarmFactory();
		return INSTANCE;
	}

	private MowSwarmFactory(){
		//to prevent direct call to public constructor
	}

	/**
	 * 
	 * @param processorClass class of processor to use, can be injected in runtime
	 * @param numberOfMowerToCreate
	 * @return
	 * @throws MowException in case of mower creation failure
	 */
	public List<Mower> createMower(Class<? extends MowProcessor> processorClass,int numberOfMowerToCreate) throws MowException{
		MowProcessor processor = null;
		LOG.info("Creating {} mowers with provided processor class \"{}\"",new Object[]{numberOfMowerToCreate,processorClass.getSimpleName()});
		if(mowerList == null){
			mowerList = new ArrayList<>();
		}
		else if(!mowerList.isEmpty()){
			throw new MowException("Should destroy all the mower before creating a new one");
		}
		for(int i = 0 ;i < numberOfMowerToCreate; i++){
			try {
				processor = processorClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new MowException("Should destroy all the mower before creating a new one",e);
			}
			Mower mower = new Mower();
			mower.setMowerName("Mower "+(i+1));
			mower.setProcessor(processor);
			mowerList.add(mower);
		}
		return mowerList;
	}

	public List<Mower> createMower(Class<? extends MowProcessor> processorClass) throws MowException{
		LOG.debug("Calling default mower creation method, instancing up to {} by default",DEFAULT_MOWER_NUMBER);
		return createMower(processorClass,DEFAULT_MOWER_NUMBER);
	}

	public void destroyMowers(){
		LOG.error("Destroying list of mowers");
		if(mowerList!=null && mowerList.size()!=0){
			mowerList.clear();
		}
	}

}
