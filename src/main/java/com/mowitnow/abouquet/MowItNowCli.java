package com.mowitnow.abouquet;

import java.util.List;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowSwarmFactory;
import com.mowitnow.abouquet.api.Mower;
import com.mowitnow.abouquet.business.SmartMowProcessor;

/**
 * 
 * @author Anthony BOUQUET
 * @since 0.0.1
 *
 */
public class MowItNowCli {

	public static void main(String[] args) {
		
		MowSwarmFactory swarmFactory = MowSwarmFactory.getInstance();
		List<Mower> mowersList;
		try {
			mowersList = swarmFactory.createMower(SmartMowProcessor.class, 2);
			
			//Use Unit Test instead of this main method to check the result of this project
		
//			Mower firstMow = mowersList.get(0);
//			firstMow.setMowerName("buzzmower");
//			MowVector firstMowVector = new MowVector();
//			firstMowVector.setOrientation(MowOrientation.NORTH);
//			firstMowVector.setPosition(new MowPosition(1,2));
//			firstMow.setMowVector(firstMowVector);
//	
//			Mower secondMow = mowersList.get(1);
//			secondMow.setMowerName("gonkmower");
//			MowVector secondMowVector = new MowVector();
//			secondMowVector.setOrientation(MowOrientation.EAST);
//			secondMowVector.setPosition(new MowPosition(3, 3));
//			secondMow.setMowVector(secondMowVector);
			

		} catch (MowException e1) {
			e1.printStackTrace();
		}
		
	}
}
