package com.mowitnow.abouquet;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowInstruction;
import com.mowitnow.abouquet.api.MowOrientation;
import com.mowitnow.abouquet.api.MowPosition;
import com.mowitnow.abouquet.api.MowSwarmFactory;
import com.mowitnow.abouquet.api.MowVector;
import com.mowitnow.abouquet.api.Mower;
import com.mowitnow.abouquet.business.SmartMowProcessor;

/**
 * Test class to validate MowItNow! work
 * @author Anthony BOUQUET
 *
 */
public class MowTest extends AbstractMowTest{
	
	private static final int NUMBER_OF_MOW_TO_CREATE = 2;
	
	//5x5 area
	private MowPosition MAX_AREA_OF_MOWING = new MowPosition(5, 5);
	
	@Test
	public void testForTwoMower() {
		MowSwarmFactory swarmFactory = MowSwarmFactory.getInstance();
		List<Mower> mowersList;
		try {
			//Creating two mowers delegating to factory the instantiation of the MowProcessors
			mowersList = swarmFactory.createMower(SmartMowProcessor.class, NUMBER_OF_MOW_TO_CREATE);
			
			//Extract the first and second one to be able to set some tests values
			Mower firstMow = mowersList.get(0);
			Mower secondMow = mowersList.get(1);

			try{
				firstMow.setMowerName("buzzmower");
				MowVector firstMowVector = new MowVector();
				firstMowVector.setOrientation(MowOrientation.NORTH);
				firstMowVector.setPosition(new MowPosition(1,2));
				firstMow.setMowVector(firstMowVector);
				firstMow.setAreaOfMowing(MAX_AREA_OF_MOWING);
				
				//Instructions given : GAGAGAGAA
				firstMow.addInstruction(MowInstruction.G);
				firstMow.addInstruction(MowInstruction.A);
				firstMow.addInstruction(MowInstruction.G);
				firstMow.addInstruction(MowInstruction.A);
				firstMow.addInstruction(MowInstruction.G);
				firstMow.addInstruction(MowInstruction.A);
				firstMow.addInstruction(MowInstruction.G);
				firstMow.addInstruction(MowInstruction.A);
				firstMow.addInstruction(MowInstruction.A);
				MowVector finalFirstMowVector = firstMow.mow();
				
				//First mower test assertion [1,3,N]
				Assert.assertEquals(finalFirstMowVector.getPosition().getPositionX(), 1);
				Assert.assertEquals(finalFirstMowVector.getPosition().getPositionY(), 3);
				Assert.assertEquals(finalFirstMowVector.getOrientation(), MowOrientation.NORTH);
			}
			catch(MowException e){
				e.printStackTrace();
			}

			try{
				secondMow.setMowerName("gonkmower");
				MowVector secondMowVector = new MowVector();
				secondMowVector.setOrientation(MowOrientation.EAST);
				secondMowVector.setPosition(new MowPosition(3, 3));
				secondMow.setMowVector(secondMowVector);
				
				//Instructions given : AADAADADDA
				secondMow.addInstruction(MowInstruction.A);
				secondMow.addInstruction(MowInstruction.A);
				secondMow.addInstruction(MowInstruction.D);
				secondMow.addInstruction(MowInstruction.A);
				secondMow.addInstruction(MowInstruction.A);
				secondMow.addInstruction(MowInstruction.D);
				secondMow.addInstruction(MowInstruction.A);
				secondMow.addInstruction(MowInstruction.D);
				secondMow.addInstruction(MowInstruction.D);
				secondMow.addInstruction(MowInstruction.A);
				
				MowVector finalSecondMowVector = secondMow.mow();
				
				//Second mower test assertion  [5,1,E]
				Assert.assertEquals(finalSecondMowVector.getPosition().getPositionX(), 5);
				Assert.assertEquals(finalSecondMowVector.getPosition().getPositionY(), 1);
				Assert.assertEquals(finalSecondMowVector.getOrientation(), MowOrientation.EAST);
			}
			catch(MowException e){
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}

		} catch (MowException e1) {
			e1.printStackTrace();
			Assert.fail(e1.getMessage());
		}
	}
}
