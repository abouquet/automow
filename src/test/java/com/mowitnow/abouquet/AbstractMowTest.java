package com.mowitnow.abouquet;

import org.testng.annotations.BeforeMethod;

import com.mowitnow.abouquet.api.MowSwarmFactory;

/**
 * Utility base class to make sure that mow factory hasn't created mower yet <br>
 * Should be called beetween tests
 * @author Anthony BOUQUET
 * 
 *
 */
public abstract class AbstractMowTest {
	
	@BeforeMethod
	public void setup(){
		//Destroy mowers list declared in previous tests
		MowSwarmFactory swarmFactory = MowSwarmFactory.getInstance();
		swarmFactory.destroyMowers();
	}
}
