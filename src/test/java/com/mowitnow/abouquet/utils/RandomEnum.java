package com.mowitnow.abouquet.utils;

import java.util.Random;

/**
 * Utility class to randomize any Enum
 * @author Anthony BOUQUET
 *
 * @param <E> Enum type
 */
public class RandomEnum<E extends Enum<?>> {

        private static final Random RND = new Random();
        private final E[] values;

        public RandomEnum(Class<E> token) {
            values = token.getEnumConstants();
        }

        public E random() {
            return values[RND.nextInt(values.length)];
        }
    }