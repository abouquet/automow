package com.mowitnow.abouquet;

import java.util.List;

import org.testng.annotations.Test;

import com.mowitnow.abouquet.api.MowException;
import com.mowitnow.abouquet.api.MowInstruction;
import com.mowitnow.abouquet.api.MowSwarmFactory;
import com.mowitnow.abouquet.api.Mower;
import com.mowitnow.abouquet.business.SmartMowProcessor;
import com.mowitnow.abouquet.utils.RandomEnum;

public class MowSwarmFactoryTest extends AbstractMowTest{
	
	private static final RandomEnum<MowInstruction> r =
	        new RandomEnum<MowInstruction>(MowInstruction.class);

  @Test(enabled=false)
  public void createMower() {
		MowSwarmFactory swarmFactory = MowSwarmFactory.getInstance();
		List<Mower> mowersList = null;
		
		try {
			//Creating two mowers delegating to factory the instantiation of the MowProcessors
			mowersList = swarmFactory.createMower(SmartMowProcessor.class, 1000);
			for(Mower m : mowersList){
				try {
					//Randomize 5 instructions
					for(int i = 0 ; i<5 ; i++){
						m.addInstruction(r.random());
					}
					m.mow();
				} catch (MowException e1) {
					e1.printStackTrace();
				}
			}
		}
		catch(MowException e){
			
		}
  }
}
